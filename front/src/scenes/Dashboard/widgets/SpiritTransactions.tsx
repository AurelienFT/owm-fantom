import {
  Paper,
  Box,
  Typography,
  TableCell,
  Table,
  TableRow,
  TableHead,
  TableBody,
  makeStyles,
  Theme,
  Grid,
  Avatar,
  TableContainer,
  TablePagination,
} from "@material-ui/core"
import React, { useState } from "react"
import { ResponsivePie } from "@nivo/pie"
import { useQuery } from "@apollo/client"
import { GET_WIDGET_DATA } from "../../../services/api/querys"
import BigNumber from "bignumber.js"
import LoadingSpinner from "../../../components/misc/LoadingSpinner"
import dayjs from "dayjs"
import ArrowUpwardIcon from "@material-ui/icons/ArrowUpward"
import ArrowDownwardIcon from "@material-ui/icons/ArrowDownward"

function stableSort<T>(array: T[]) {
  const stabilizedThis = array.map((el, index) => [el, index] as [T, number])
  return stabilizedThis.map((el) => el[0])
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    width: "100%",
  },
  paper: {
    width: "100%",
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
}))

export default function SpiritTransactions() {
  const classes = useStyles()
  const [data, setData] = useState<any[]>([])
  const widgetData = useQuery(GET_WIDGET_DATA, {
    variables: { data: { name: "SpiritSwap transactions" } },
    onCompleted: async (res) => {
      let result: any[] = JSON.parse(res.widget.result)
      result = result.reverse()
      result = result.filter((elem) => {
        return elem.act !== "SWAP"
      })
      setData(result)
    },
  })

  const [page, setPage] = React.useState(0)

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage)
  }

  if (widgetData.loading) return <LoadingSpinner />
  return (
    <Box className={classes.root} style={{ height: "500px", width: "500px" }}>
      <Paper className={classes.paper}>
        <TableContainer>
          <Box pb={2} style={{ textAlign: "center" }}>
            <Typography component="h1">SpiritSwap Transactions</Typography>
          </Box>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>Date</TableCell>
                <TableCell>Type</TableCell>
                <TableCell>Pool</TableCell>
                <TableCell>First token amount</TableCell>
                <TableCell>Second token amount</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {stableSort(data)
                .slice(page * 5, page * 5 + 5)
                .map((row: any, index) => {
                  return (
                    <TableRow key={index}>
                      <TableCell style={{ color: "black" }} component="th" scope="row">
                        {dayjs(row.block_signed_at).format("LTS")}
                      </TableCell>
                      <TableCell component="th" scope="row">
                        {row.act}
                      </TableCell>
                      <TableCell component="th" scope="row">
                        {row.token_0.contract_ticker_symbol} / {row.token_1.contract_ticker_symbol}
                      </TableCell>
                      <TableCell align="left" component="th" scope="row">
                        {row.amount_0 / 10 ** row.token_0.contract_decimals}
                      </TableCell>
                      <TableCell align="left" component="th" scope="row">
                        {row.amount_1 / 10 ** row.token_1.contract_decimals}
                      </TableCell>
                    </TableRow>
                  )
                })}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
      <TablePagination
        rowsPerPageOptions={[5]}
        component="div"
        count={data.length}
        rowsPerPage={5}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={() => null}
      />
    </Box>
  )
}

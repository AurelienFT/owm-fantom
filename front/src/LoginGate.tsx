import React from "react"
import useLocalStorage from "react-use-localstorage"

import PrivateApp from "./PrivateApp"
import PublicApp from "./PublicApp"

const LoginGate = (): JSX.Element => {
  const [accessToken] = useLocalStorage("accessToken", "")
  if (accessToken.length > 0) {
    return <PrivateApp user={accessToken} />
  } else {
    return <PublicApp />
  }
}

export default LoginGate

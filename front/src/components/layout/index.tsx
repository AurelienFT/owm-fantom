import { AppBar, Toolbar, Box } from "@material-ui/core/"
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles"
import React from "react"

import ChildrenProps from "../../services/misc/childrenProps"
import PrivateLayout from "./PrivateLayout"
import PublicLayout from "./PublicLayout"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      height: "100vh",
      display: "flex",
      flexDirection: "column",
    },
    grow: {
      flexGrow: 1,
    },
    title: {
      width: "145px",
      display: "flex",
      justifyContent: "center",
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(["margin", "width"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      height: "62px",
      display: "flex",
      justifyContent: "center",
      paddingLeft: theme.spacing(6),
      paddingRight: theme.spacing(6),
    },
    toolBar: {
      height: "62px",
    },
    logo: {
      maxWidth: "145px",
    },
    warningBanner: {
      backgroundColor: "red",
      width: "100%",
      height: "32px",
      color: "white",
      textAlign: "center",
      fontWeight: "bold",
    },
  }),
)

interface Props extends ChildrenProps {
  user?: string
}

const AppLayout = ({ user, children }: Props): JSX.Element => {
  const classes = useStyles()
  return (
    <div className={classes.root}>
      {/* TODO replace with privateLayout */}
      <div>
        <AppBar position="relative" elevation={2} color="primary" className={classes.appBar}>
          <Toolbar variant="dense" className={classes.toolBar}>
            {user ? <PrivateLayout /> : <PublicLayout />}
          </Toolbar>
        </AppBar>
      </div>
      {user && (
        <div className={classes.warningBanner}>
          <Box pt={1}>Fetching data can take up to 30 seconds</Box>
        </div>
      )}
      <main className={classes.grow}>{children}</main>
    </div>
  )
}

export default AppLayout

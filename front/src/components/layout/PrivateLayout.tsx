import { Avatar, Divider, IconButton, MenuItem, Popover, Typography } from "@material-ui/core"
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles"
import { ExitToAppRounded } from "@material-ui/icons"
import React from "react"
import { useHistory } from "react-router-dom"
import useLocalStorage from "react-use-localstorage"
import SettingsIcon from "@material-ui/icons/Settings"
import { LightGrey, White } from "../../materialTheme"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    avatar: {
      "color": theme.palette.getContrastText(LightGrey),
      "backgroundColor": LightGrey,
      "width": theme.spacing(4),
      "height": theme.spacing(4),
      "&:hover": {
        color: theme.palette.getContrastText(White),
        backgroundColor: White,
      },
    },
    unclickableLink: {
      cursor: "default",
      color: `${theme.palette.text.disabled}`,
    },
    linkTypographyWithIcon: {
      marginLeft: "0.5em",
      fontWeight: 400,
    },
    linkTypography: {
      marginLeft: theme.spacing(0.5),
      fontWeight: 400,
    },
    sectionDesktop: {
      display: "none",
      [theme.breakpoints.up("md")]: {
        display: "flex",
      },
      height: "62px",
    },
    menuPaper: {
      marginTop: theme.spacing(1),
      backgroundColor: theme.palette.primary.main,
      color: LightGrey,
    },
    activeIcon: {
      color: theme.palette.action.selected,
    },
    notActiveIcon: {
      color: "inherit",
    },
    dividerSubLink: {
      backgroundColor: "#555",
      marginLeft: theme.spacing(2),
      marginRight: theme.spacing(2),
    },
    paper: {
      padding: theme.spacing(1),
    },
  }),
)

const PrivateLayout = (): JSX.Element => {
  const classes = useStyles()
  const history = useHistory()
  const [, setAccessToken] = useLocalStorage("accessToken", "")
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
  const [showBackDrop, setShowBackdrop] = React.useState(false)

  const isMenuOpen = Boolean(anchorEl)

  const handleProfileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const handleMenuClose = () => {
    setAnchorEl(null)
  }

  React.useEffect(() => {
    return () => {
      if (showBackDrop) setShowBackdrop(false)
    }
  }, [showBackDrop])

  const handleLogOut = async (): Promise<void> => {
    setAccessToken("")
    handleMenuClose()
    history.go(0)
  }

  const menuId = "primary-search-account-menu"
  const profileMenu = (
    <Popover
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      id={menuId}
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMenuOpen}
      onClose={handleMenuClose}
      classes={{ paper: classes.menuPaper }}
    >
      <Divider className={classes.dividerSubLink} />
      <MenuItem onClick={handleLogOut}>
        <ExitToAppRounded />
        <Typography className={classes.linkTypographyWithIcon}>Log out</Typography>
      </MenuItem>
    </Popover>
  )

  return (
    <>
      <div className={classes.sectionDesktop}>
        <IconButton
          edge="end"
          aria-label="Account of current user"
          aria-controls={menuId}
          aria-haspopup="true"
          onClick={handleProfileMenuOpen}
          className={classes.activeIcon}
        >
          <Avatar className={classes.avatar}>
            <SettingsIcon />
          </Avatar>
        </IconButton>
      </div>

      {profileMenu}
    </>
  )
}

export default PrivateLayout

import { createStyles, makeStyles, Theme } from "@material-ui/core/styles"
import React from "react"
import { useLocation } from "react-router-dom"

import { URLS } from "../../routes"
import { LinkIcon } from "./LinkIcons"

// TODO Refactor the profile menu
// TODO Refactor the workspace switcher

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    sectionDesktop: {
      display: "none",
      [theme.breakpoints.up("md")]: {
        display: "flex",
      },
      height: "62px",
    },
  }),
)

const PublicLayout = (): JSX.Element => {
  const classes = useStyles()
  const { search } = useLocation()

  return (
    <>
      <div className={classes.sectionDesktop}>
        <LinkIcon url={`${URLS.Account.Login}`} title="Login" />
        <LinkIcon url={`${URLS.Account.Register}`} title="Register" />
      </div>
    </>
  )
}

export default PublicLayout

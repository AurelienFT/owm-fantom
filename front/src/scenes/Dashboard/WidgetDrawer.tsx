import { useQuery, useLazyQuery, useMutation } from "@apollo/client"
import { useSnackbar } from "notistack"
import {
  Box,
  Button,
  Drawer,
  Switch,
  FormLabel,
  Typography,
  Divider,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@material-ui/core"
import { makeStyles, Theme } from "@material-ui/core/styles"
import React, { useState } from "react"
import { GET_WIDGETS_NAME, GET_MY_WIDGETS } from "../../services/api/querys"
import { ADD_NEW_WIDGET, REMOVE_WIDGET } from "../../services/api/mutations"
import LoadingSpinner from "../../components/misc/LoadingSpinner"
import TextField from "@material-ui/core/TextField"

interface Widgets {
  name: string
  type: string
  id: number
}

interface WidgetsChecked {
  [name: string]: boolean
}

const useStyles = makeStyles((theme: Theme) => ({
  drawerPaper: {
    width: 500,
  },
}))
export default function WidgetDrawer(props: any) {
  const classes = useStyles()
  const { enqueueSnackbar } = useSnackbar()
  const [isOpen, setIsOpen] = useState(false)
  const [openModalPrams, setOpenModalPrams] = React.useState(false)
  const [currentParam, setCurrentParam] = React.useState<string | null>(null)
  const [currentSelectedWidget, setCurrentSelectedWidget] = React.useState<Widgets | null>(null)
  const [data, setData] = useState<Widgets[]>([])
  const [dataSwitch, setDataSwitch] = useState<WidgetsChecked>({})
  const [addWidget] = useMutation(ADD_NEW_WIDGET, {
    fetchPolicy: "no-cache",
    onCompleted: () => {
      enqueueSnackbar("Successfully added the widget", { variant: "success" })
    },
    refetchQueries: [{ query: GET_MY_WIDGETS }],
  })
  const [removeWidget] = useMutation(REMOVE_WIDGET, {
    fetchPolicy: "no-cache",
    onCompleted: () => {
      enqueueSnackbar("Successfully removed the widget", { variant: "success" })
    },
    refetchQueries: [{ query: GET_MY_WIDGETS }],
  })
  const [myWidgets, myWidgetsData] = useLazyQuery(GET_MY_WIDGETS, {
    fetchPolicy: "cache-and-network",
    onCompleted: (res) => {
      const tmp: any = {}
      data.map((x) => {
        if (res.myWidgets.find((y: any) => y.widget.name === x.name)) {
          tmp[x.name] = true
        } else {
          tmp[x.name] = false
        }
      })
      setDataSwitch(tmp)
    },
  })
  const widgets = useQuery(GET_WIDGETS_NAME, {
    onCompleted: async (res) => {
      setData(res.widgets)
      myWidgets()
    },
  })

  const handleClickOpenModalParams = () => {
    setOpenModalPrams(true)
  }

  const handleCloseModalParams = () => {
    setOpenModalPrams(false)
  }
  if (widgets.loading) return <LoadingSpinner />

  const addOrRemoveWidget = (x: Widgets, addressParam?: string) => {
    if (dataSwitch[x.name] === false) {
      addWidget({
        variables: {
          data: { widgetId: x.id, x: 0, y: 0, params: JSON.stringify({ address: addressParam }) },
        },
      })
    } else {
      removeWidget({ variables: { data: { widgetId: x.id } } })
    }
    props.onSwitchChange()
  }

  return (
    <>
      <Box pl={5}>
        <Button onClick={() => setIsOpen(true)}>Open widgets list</Button>
      </Box>
      <Drawer
        classes={{
          paper: classes.drawerPaper,
        }}
        anchor={"left"}
        open={isOpen}
        onClose={() => setIsOpen(false)}
      >
        <Box pt={5}>
          <Box pb={2} style={{ textAlign: "center" }}>
            <Typography component="h1">Widgets List</Typography>
          </Box>
          <Divider />
          <Box pl={5}>
            {data.map((x, index) => {
              return (
                <Box key={index} pt={2}>
                  <FormLabel component="legend">{x.name}</FormLabel>
                  <Switch
                    checked={dataSwitch[x.name]}
                    onChange={() => {
                      if (!dataSwitch[x.name]) {
                        handleClickOpenModalParams()
                        setCurrentSelectedWidget(x)
                      } else {
                        addOrRemoveWidget(x)
                        const cpySwitch = dataSwitch
                        cpySwitch[x.name] = !cpySwitch[x.name]
                        setDataSwitch(cpySwitch)
                      }
                    }}
                  />
                </Box>
              )
            })}
          </Box>
        </Box>
      </Drawer>
      <Dialog open={openModalPrams} onClose={handleCloseModalParams} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Params</DialogTitle>
        <DialogContent>
          <DialogContentText>To use this widget you need to fill out this parameters.</DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="address"
            label="Your Address"
            type="text"
            fullWidth
            onChange={(e) => {
              setCurrentParam(e.target.value)
            }}
          />
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => {
              handleCloseModalParams()
            }}
            color="primary"
          >
            Cancel
          </Button>
          <Button
            onClick={(e) => {
              handleCloseModalParams()
              const cpySwitch = dataSwitch
              if (currentSelectedWidget && currentParam) {
                addOrRemoveWidget(currentSelectedWidget, currentParam)
                cpySwitch[currentSelectedWidget.name] = !cpySwitch[currentSelectedWidget.name]
                setDataSwitch(cpySwitch)
              }
            }}
            color="primary"
          >
            Submit
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
}

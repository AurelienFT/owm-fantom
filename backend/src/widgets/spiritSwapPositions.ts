import { User, Prisma } from '@prisma/client'
import {WidgetResult, TypeWidgetResult} from '../interfaces/Widget'
import routes from '../routes.json'
import fetch from "node-fetch";

export default async (_user: User, params: Prisma.JsonObject): Promise<WidgetResult> => {
    const response = await fetch(`${routes.root}/${routes.spiritSwapPositions}/${params["address"]}/balances/?key=${routes.apiKey}&page-size=99999`,  {
        method: "GET",
    });
    const object = await response.json();
    if (!object.data || !object.data.pancakeswap) {
        throw new Error("Can't load widget with this address.")
    }
    return {
        id: 4,
        name: "SpiritSwap positions",
        result: JSON.stringify(object.data.pancakeswap.balances),
        type: TypeWidgetResult.ARRAY
    }
}

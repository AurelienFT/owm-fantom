import {
  Paper,
  Box,
  Typography,
  TableCell,
  Table,
  TableRow,
  TableHead,
  TableBody,
  makeStyles,
  Theme,
  Grid,
  Avatar,
  TableContainer,
  TablePagination,
} from "@material-ui/core"
import React, { useState } from "react"
import { ResponsivePie } from "@nivo/pie"
import { useQuery } from "@apollo/client"
import { GET_WIDGET_DATA } from "../../../services/api/querys"
import BigNumber from "bignumber.js"
import LoadingSpinner from "../../../components/misc/LoadingSpinner"
import dayjs from "dayjs"
import ArrowUpwardIcon from "@material-ui/icons/ArrowUpward"
import ArrowDownwardIcon from "@material-ui/icons/ArrowDownward"

function stableSort<T>(array: T[]) {
  const stabilizedThis = array.map((el, index) => [el, index] as [T, number])
  return stabilizedThis.map((el) => el[0])
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    width: "100%",
  },
  paper: {
    width: "100%",
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
}))

interface Transfer {
  date: string
  currency: string
  currencyTicker: string
  currencyLogo: string
  amount: Number
  inOrOut: string
  inOrOutAddress: string
}
export default function Transfers() {
  const classes = useStyles()
  const [data, setData] = useState<Transfer[]>([])
  const widgetData = useQuery(GET_WIDGET_DATA, {
    variables: { data: { name: "Transfers" } },
    onCompleted: async (res) => {
      const result = JSON.parse(res.widget.result)
      setData(result)
    },
  })

  const [page, setPage] = React.useState(0)

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage)
  }

  if (widgetData.loading) return <LoadingSpinner />
  return (
    <Box className={classes.root} style={{ height: "500px", width: "500px" }}>
      <Paper className={classes.paper}>
        <TableContainer>
          <Box pb={2} style={{ textAlign: "center" }}>
            <Typography component="h1">Transfers</Typography>
          </Box>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>Date</TableCell>
                <TableCell>Type</TableCell>
                <TableCell>Asset</TableCell>
                <TableCell align={"center"}>Amount</TableCell>
                <TableCell align={"center"}>To/From</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {stableSort(data)
                .slice(page * 5, page * 5 + 5)
                .map((row: any, index) => {
                  return (
                    <TableRow key={index}>
                      <TableCell style={{ color: "black" }} component="th" scope="row">
                        {dayjs(row.date).format("LTS")}
                      </TableCell>
                      <TableCell component="th" scope="row">
                        {row.inOrOut === "IN" ? (
                          <ArrowUpwardIcon style={{ color: "green" }} />
                        ) : (
                          <ArrowDownwardIcon style={{ color: "red" }} />
                        )}
                      </TableCell>
                      <TableCell style={{ color: "black" }} component="th" scope="row">
                        <Avatar alt={row.currencyTicker} src={row.currencyLogo} />
                        <Box pl={1} pt={1}>
                          {row.currencyTicker}
                        </Box>
                      </TableCell>
                      <TableCell align="left" style={{ color: "black" }} component="th" scope="row">
                        {row.amount}
                      </TableCell>
                      <TableCell style={{ color: "black" }} component="th" scope="row">
                        {row.inOrOutAddress}
                      </TableCell>
                    </TableRow>
                  )
                })}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
      <TablePagination
        rowsPerPageOptions={[5]}
        component="div"
        count={data.length}
        rowsPerPage={5}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={() => null}
      />
    </Box>
  )
}

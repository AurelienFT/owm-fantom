import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

async function main() {
    await prisma.widget.create({
        data: {
            name: "Balance",
            type: "ARRAY"
        }
    });
    await prisma.widget.create({
        data: {
            name: "Wallet value over the month",
            type: "GRAPHIC"
        }
    });
    await prisma.widget.create({
        data: {
            name: "SpiritSwap positions",
            type: "ARRAY"
        }
    });
    await prisma.widget.create({
        data: {
            name: "SpiritSwap transactions",
            type: "ARRAY"
        }
    });
    await prisma.widget.create({
        data: {
            name: "Transfers",
            type: "ARRAY"
        }
    });
}

main()
    .catch(e => {
        console.error(e)
        process.exit(1)
    })
    .finally(async () => {
        await prisma.$disconnect()
    })
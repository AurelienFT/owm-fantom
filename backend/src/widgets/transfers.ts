import { User, Prisma } from '@prisma/client'
import { WidgetResult, TypeWidgetResult } from '../interfaces/Widget'
import routes from '../routes.json'
import fetch from "node-fetch";
import tokens from "../tokens.json";
/// Params must be :
/// {
///    "address": "0xeeeeeeeeeeee"
/// }

type Transfer = {
    date: Date
    currency: String
    currencyTicker: String
    currencyLogo: String
    amount: Number
    inOrOut: String
    inOrOutAddress: String
};

export default async (_user: User, params: Prisma.JsonObject): Promise<WidgetResult> => {
    let transfers: Transfer[] = [];
    for (let token of tokens) {
        console.log(token);
        const response = await fetch(`${routes.root}/address/${params["address"]}/${routes.getTransfers}/?contract-address=${token.contract_address}&key=${routes.apiKey}&page-size=99999`, {
            method: "GET",
        });
        const object = await response.json();
        if (!object.data || !object.data.items) {
            throw new Error("Can't load widget with this address.")
        }
        for (let item of object.data.items) {
            if (!item.transfers.length) {
                continue;
            }
            transfers.push({
                date: new Date(item.block_signed_at),
                currency: item.transfers[0].contract_name,
                currencyTicker: item.transfers[0].contract_ticker_symbol,
                currencyLogo: item.transfers[0].logo_url,
                amount: item.transfers[0].delta / 10 ** item.transfers[0].contract_decimals,
                inOrOut: item.transfers[0].transfer_type,
                inOrOutAddress: item.transfers[0].transfer_type === "IN" ? item.transfers[0].from_address : item.transfers[0].to_address
            });
        }
    }
    transfers = transfers.sort((a, b) => a.date < b.date ? 1 : -1);
    return {
        id: 6,
        name: "transactions",
        result: JSON.stringify(transfers),
        type: TypeWidgetResult.ARRAY
    }
}

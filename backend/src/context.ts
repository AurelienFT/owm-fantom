import { PrismaClient, User } from '@prisma/client'
import http from 'http';
import { type } from 'os';
import jsonwebtoken from 'jsonwebtoken'

const prisma = new PrismaClient()

export interface Context {
  prisma: PrismaClient,
  user: User | null | undefined
}

export async function init(headers: http.IncomingHttpHeaders): Promise<Context> {
  if (!headers["authorization"]) {
    return {
      prisma: prisma,
      user: null
    }
  }
  let content = jsonwebtoken.decode(headers["authorization"]);
  if (!content) {
    throw Error("The authorization header you provided is not well formatted.")
  }
  let context: Context = {
    prisma: prisma,
    user: headers["authorization"] != undefined ? await prisma.user.findUnique({where: {id: parseInt(content as string) }}) : undefined
  }
  return context;
}
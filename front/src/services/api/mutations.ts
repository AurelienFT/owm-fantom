import { gql } from "@apollo/client"

const CREATE_ACCOUNT_WITH_EMAIL = gql`
  mutation signupUser($data: UserCreateInput!) {
    signupUser(data: $data) {
      user {
        email
        name
        id
      }
      jwt
    }
  }
`

const ADD_NEW_WIDGET = gql`
  mutation addNewWidget($data: UserWidgetInput!) {
    addNewWidget(data: $data) {
      __typename
    }
  }
`

const REMOVE_WIDGET = gql`
  mutation removeWidget($data: UserWidgetDeleteInput!) {
    removeWidget(data: $data) {
      __typename
    }
  }
`
const UPDATE_POSITIONS_WIDGET = gql`
  mutation updatePositionsUserWidget($data: UserWidgetPositionsInput!) {
    updatePositionsUserWidget(data: $data) {
      __typename
    }
  }
`

export { CREATE_ACCOUNT_WITH_EMAIL, ADD_NEW_WIDGET, REMOVE_WIDGET, UPDATE_POSITIONS_WIDGET }

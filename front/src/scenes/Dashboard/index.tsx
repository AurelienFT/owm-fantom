import { Box, Paper } from "@material-ui/core"
import { makeStyles, Theme } from "@material-ui/core/styles"
import React, { useState, useEffect } from "react"
import { GET_MY_WIDGETS } from "../../services/api/querys"
import { UPDATE_POSITIONS_WIDGET } from "../../services/api/mutations"
// @ts-ignore
import GridLayout from "react-grid-layout"

import WidgetDrawer from "./WidgetDrawer"
import Balance from "./widgets/Balances"
import Position from "./widgets/Position"
import Transfers from "./widgets/Transfers"
import WalletValue from "./widgets/WalletValue"
import SpiritTransactions from "./widgets/SpiritTransactions"
import { useLazyQuery, useMutation } from "@apollo/client"
import LoadingSpinner from "../../components/misc/LoadingSpinner"

interface TypeWidgetActivation {
  [name: string]: boolean
}

const orderedWidget = [
  "Balance",
  "Wallet value over the month",
  "SpiritSwap positions",
  "Transfers",
  "SpiritSwap transactions",
]

const useStyles = makeStyles((theme: Theme) => ({}))
export default function Dashboard() {
  const classes = useStyles()
  const [firstLayout, setFirstLayout] = useState([
    { i: "Balance", x: 0, y: 0, w: 3, h: 8, isResizable: false, component: <Balance /> },
    { i: "Wallet value over the month", x: 0, y: 0, w: 5, h: 10, isResizable: false, component: <WalletValue /> },
    { i: "SpiritSwap positions", x: 0, y: 0, w: 3, h: 8, isResizable: false, component: <Position /> },
    {
      i: "SpiritSwap transactions",
      x: 0,
      y: 0,
      w: 3,
      h: 11,
      isResizable: false,
      component: <SpiritTransactions />,
    },
    { i: "Transfers", x: 0, y: 0, w: 3, h: 17, isResizable: false, component: <Transfers /> },
  ])
  const [widgetChecked, setWidgetChecked] = useState<TypeWidgetActivation>({
    "Balance": false,
    "Wallet value over the month": false,
    "SpiritSwap positions": false,
    "SpiritSwap transactions": false,
    "Transfers": false,
  })
  const [myWidgets, myWidgetsData] = useLazyQuery(GET_MY_WIDGETS, {
    fetchPolicy: "network-only",
    notifyOnNetworkStatusChange: true,
    onCompleted: (res) => {
      console.log("res", res)
      if (!res) return undefined
      for (const widget of res.myWidgets) {
        const i = firstLayout.findIndex((x) => x.i === widget.widget.name)
        if (firstLayout[i]) {
          firstLayout[i].x = widget.x
          firstLayout[i].y = widget.y
        }
      }
      setFirstLayout(firstLayout)
      setWidgetChecked({
        "Balance": res.myWidgets.find((x: any) => x.widget.name == "Balance") !== undefined,
        "SpiritSwap positions": res.myWidgets.find((x: any) => x.widget.name == "SpiritSwap positions") !== undefined,
        "SpiritSwap transactions":
          res.myWidgets.find((x: any) => x.widget.name == "SpiritSwap transactions") !== undefined,
        "Wallet value over the month":
          res.myWidgets.find((x: any) => x.widget.name == "Wallet value over the month") !== undefined,
        "Transfers": res.myWidgets.find((x: any) => x.widget.name == "Transfers") !== undefined,
      })
    },
  })
  useEffect(() => {
    myWidgets()
  }, [])
  const [updatePositions] = useMutation(UPDATE_POSITIONS_WIDGET, {})
  const onLayoutChange = (layoutParam: any[]) => {
    for (const layoutElem of layoutParam) {
      updatePositions({
        variables: { data: { x: layoutElem.x, y: layoutElem.y, widgetId: orderedWidget.indexOf(layoutElem.i) + 1 } },
      })
    }
  }
  if (myWidgetsData.loading === true) return <LoadingSpinner />
  const oneWidgetIsHere = () =>
    widgetChecked.Balance === true ||
    widgetChecked["SpiritSwap positions"] === true ||
    widgetChecked["SpiritSwap transactions"] === true ||
    widgetChecked["Wallet value over the month"] === true ||
    widgetChecked["Transfers"] === true
  const layoutFiltered = firstLayout.filter((elem) => {
    return widgetChecked[elem.i] === true
  })

  return (
    <>
      <Box pt={20}>
        <WidgetDrawer onSwitchChange={myWidgets} />
        {oneWidgetIsHere() && (
          <GridLayout
            verticalCompact={false}
            className="layout"
            layout={layoutFiltered}
            cols={12}
            rowHeight={30}
            width={1980}
            onLayoutChange={onLayoutChange}
          >
            {layoutFiltered.map(function (elem, index) {
              return <div key={elem.i}>{elem.component}</div>
            })}
          </GridLayout>
        )}
      </Box>
    </>
  )
}

export interface LoginInput {
    email: string,
    password: string
}

export interface UserCreateInput {
    email: string,
    name?: string,
    password: string
}

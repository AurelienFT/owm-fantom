import { Paper, Box, Typography } from "@material-ui/core"
import React, { useState } from "react"
import { ResponsivePie } from "@nivo/pie"
import { useQuery } from "@apollo/client"
import { GET_WIDGET_DATA } from "../../../services/api/querys"
import BigNumber from "bignumber.js"
import LoadingSpinner from "../../../components/misc/LoadingSpinner"

export default function Positon() {
  const [data, setData] = useState([])
  const widgetData = useQuery(GET_WIDGET_DATA, {
    variables: { data: { name: "SpiritSwap positions" } },
    onCompleted: async (res) => {
      const result = JSON.parse(res.widget.result)
      let clean: any = []
      result.map((x: any) => {
        for (const key in x) {
          clean = clean.concat([
            {
              label: x[key].contract_ticker_symbol,
              id: x[key].contract_ticker_symbol,
              value:
                x[key].contract_decimals < 1
                  ? new BigNumber(x[key].balance).decimalPlaces(4, BigNumber.ROUND_UP).toNumber()
                  : new BigNumber(x[key].balance)
                      .shiftedBy(-x[key].contract_decimals)
                      .decimalPlaces(4, BigNumber.ROUND_UP)
                      .toNumber(),
            },
          ])
        }
      })
      clean = clean.filter((x: any) => x.value > 0).slice(0, 10)
      setData(clean)
    },
  })
  if (widgetData.loading) return <LoadingSpinner />
  return (
    <Paper style={{ width: 500 }}>
      <Box style={{ height: "300px" }}>
        <Box pb={2} style={{ textAlign: "center" }}>
          <Typography component="h1">SpiritSwap positions</Typography>
        </Box>
        <ResponsivePie
          theme={{
            legends: { hidden: { symbol: { fill: "#d9d9d9" } } },
          }}
          data={data}
          margin={{ top: 40, right: 40, bottom: 40, left: 40 }}
          colors={{ scheme: "spectral" }}
          innerRadius={0.6}
          padAngle={0.7}
          cornerRadius={3}
          activeOuterRadiusOffset={8}
          borderWidth={1}
          borderColor={{ from: "color", modifiers: [["darker", 0.2]] }}
          enableArcLinkLabels={false}
          enableArcLabels={false}
          legends={[
            {
              anchor: "right",
              direction: "column",
              justify: false,
              translateX: 25,
              translateY: 0,
              itemsSpacing: 10,
              itemWidth: 100,
              itemHeight: 15,
              itemDirection: "left-to-right",
              itemOpacity: 1,
              symbolSize: 18,
              symbolShape: "circle",
              toggleSerie: true,
              effects: [
                {
                  on: "hover",
                  style: {
                    itemTextColor: "#999",
                  },
                },
              ],
            },
          ]}
        />
      </Box>
    </Paper>
  )
}

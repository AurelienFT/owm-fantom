import { User, Prisma } from '@prisma/client'
import {WidgetResult, TypeWidgetResult} from '../interfaces/Widget'
import routes from '../routes.json'
import fetch from "node-fetch";

export default async (_user: User, params: Prisma.JsonObject): Promise<WidgetResult> => {
    const response = await fetch(`${routes.root}/address/${params["address"]}/${routes.getPortfolio}/?key=${routes.apiKey}&page-size=99999`,  {
        method: "GET",
    });
    const object = await response.json();
    if (!object.items) {
        throw new Error("Can't load widget with this address.")
    }
    let returnedHoldings: any = {};
    for (let item of object.items) {
        console.log(item.contract_name)
        for (let holding of item.holdings) {
            if (holding.close.quote && holding.close.quote < 0)
                continue
            if (returnedHoldings[holding.timestamp]) {
                if (holding.close.quote)
                    returnedHoldings[holding.timestamp] = Number(returnedHoldings[holding.timestamp]) + Number(holding.close.quote);
            } else {
                holding.close.quote ? returnedHoldings[holding.timestamp] = Number(holding.close.quote) : returnedHoldings[holding.timestamp] = 0;
            }
            console.log(holding);
        }
    }
    return {
        id: 3,
        name: "Wallet value over the month",
        result: JSON.stringify(returnedHoldings),
        type: TypeWidgetResult.GRAPHIC
    }
}

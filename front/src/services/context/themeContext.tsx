import { PaletteType, ThemeProvider } from "@material-ui/core"
import React from "react"

import getTheme from "../../materialTheme"
import ChildrenProps from "../misc/childrenProps"

interface ContextValue {
  type: PaletteType
}

export const ThemeContext =
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  React.createContext<ContextValue>((undefined as any) as ContextValue)

type AllProps = ChildrenProps

export const ThemeContextProvider = (props: AllProps): JSX.Element => {
  return (
    <ThemeContext.Provider
      value={{
        type: "light",
      }}
    >
      <ThemeProvider theme={getTheme("light")}>{props.children}</ThemeProvider>
    </ThemeContext.Provider>
  )
}

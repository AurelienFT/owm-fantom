import { User, Prisma } from '@prisma/client'
import {WidgetResult, TypeWidgetResult} from '../interfaces/Widget'
import routes from '../routes.json'
import fetch from "node-fetch";
/// Params must be :
/// {
///    "address": "0xeeeeeeeeeeee"
/// }
export default async (_user: User, params: Prisma.JsonObject): Promise<WidgetResult> => {
    const response = await fetch(`${routes.root}/address/${params["address"]}/${routes.getBalance}/?key=${routes.apiKey}&page-size=99999`,  {
        method: "GET",
    });
    const object = await response.json();
    if (!object.data || !object.data.items) {
        throw new Error("Can't load widget with this address.")
    }
    return {
        id: 1,
        name: "balance",
        result: JSON.stringify(object.data.items),
        type: TypeWidgetResult.ARRAY
    }
}

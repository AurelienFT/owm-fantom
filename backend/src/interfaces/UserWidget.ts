export interface UserWidgetInput {
    widgetId: number
    x: number
    y: number
    params: string
}

export interface UserWidgetDeleteInput {
    widgetId: number
}

export interface UserWidgetPositionsInput {
    widgetId: number
    x: number
    y: number
}
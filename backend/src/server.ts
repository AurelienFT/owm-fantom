import express from 'express'
import { graphqlHTTP } from 'express-graphql'
import { schema } from './schema'
import { Context, init } from './context'
const cors = require('cors');

const app = express()

app.use((req, res, next) => {
  console.log(req.headers)
  next();
});

app.use("/graphql", function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
  if (req.method === 'OPTIONS') {
    res.sendStatus(200);
  } else {
    next();
  }
});


app.use(
  '/graphql',
  cors(),
  graphqlHTTP(async (request, response, graphQLParams) => ({
    schema,
    context: await init(request.headers),
    graphiql: true,
  })),
)

app.listen(4000)
console.log(`\
🚀 Server ready at: http://localhost:4000/graphql
⭐️ See sample queries: http://pris.ly/e/ts/graphql-express-sdl-first#using-the-graphql-api
`)
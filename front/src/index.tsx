import "react-grid-layout/css/styles.css"
import "react-resizable/css/styles.css"
import { ApolloClient, ApolloProvider, InMemoryCache } from "@apollo/client"
import DayJsUtils from "@date-io/dayjs"
import { CssBaseline, NoSsr } from "@material-ui/core"
import { MuiPickersUtilsProvider } from "@material-ui/pickers"
import React from "react"
import ReactDOM from "react-dom"
import { BrowserRouter } from "react-router-dom"

import LoginGate from "./LoginGate"
import reportWebVitals from "./reportWebVitals"
import { ThemeContextProvider } from "./services/context/themeContext"
import useLocalStorage from "react-use-localstorage"
import { SnackbarProvider } from "notistack"

const App = () => {
  const [accessToken] = useLocalStorage("accessToken", "")
  const client = new ApolloClient({
    uri: "https://omwfantom.aurelienfoucault.fr:4001/graphql",
    cache: new InMemoryCache(),
    headers: {
      Authorization: accessToken,
    },
  })
  return (
    <React.StrictMode>
      <ThemeContextProvider>
        <CssBaseline />
        <NoSsr>
          <ApolloProvider client={client}>
            <BrowserRouter>
              <MuiPickersUtilsProvider utils={DayJsUtils}>
                <SnackbarProvider maxSnack={3}>
                  <LoginGate />
                </SnackbarProvider>
              </MuiPickersUtilsProvider>
            </BrowserRouter>
          </ApolloProvider>
        </NoSsr>
      </ThemeContextProvider>
    </React.StrictMode>
  )
}

ReactDOM.render(<App />, document.getElementById("root"))

reportWebVitals()

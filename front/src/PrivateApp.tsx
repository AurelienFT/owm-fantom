import React from "react"
import { Redirect, Route, Switch } from "react-router-dom"

import AppLayout from "./components/layout"
import { URLS } from "./routes"
import Dashboard from "./scenes/Dashboard"

interface Props {
  user: string // Rajouter le type
}

const PrivateApp = ({ user }: Props): JSX.Element => {
  return (
    <AppLayout user={user}>
      <Switch>
        <Route exact path={URLS.Dashboard} component={Dashboard} />
        <Redirect to={URLS.Dashboard} />
      </Switch>
    </AppLayout>
  )
}

export default PrivateApp

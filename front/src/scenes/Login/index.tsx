import { useLazyQuery } from "@apollo/client"
import { Box, Button, Grid, Paper } from "@material-ui/core"
import { makeStyles, Theme } from "@material-ui/core/styles"
import TextField from "@material-ui/core/TextField"
import EmailIcon from "@material-ui/icons/Email"
import LockIcon from "@material-ui/icons/Lock"
import React, { useState } from "react"
import { useHistory } from "react-router-dom"
import useLocalStorage from "react-use-localstorage"

import { CONNECT_WITH_EMAIL } from "../../services/api/querys"

const useStyles = makeStyles((theme: Theme) => ({
  textField: {
    borderRadius: "35px",
  },
  container: {
    margin: "0 auto",
    maxWidth: "38em",
    marginBottom: theme.spacing(3),
  },
}))

export default function Login() {
  const classes = useStyles()
  const history = useHistory()
  const [_, setAccessToken] = useLocalStorage("accessToken", "")
  const [login] = useLazyQuery(CONNECT_WITH_EMAIL, {
    onCompleted: (res) => {
      if (res.login.jwt) {
        setAccessToken(res.login.jwt)
        history.go(0)
      }
    },
    onError: () => {
      setError(true)
    },
  })
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [error, setError] = useState(false)

  const onClick = () => {
    login({
      variables: { data: { email: email, password: password } },
    })
  }

  return (
    <Box pt={20}>
      <Paper className={classes.container}>
        <Box pt={3} pb={3} style={{ textAlign: "center" }}>
          <Grid container>
            <Grid item xs={12}>
              <TextField
                defaultValue={email}
                onChange={(val) => {
                  setEmail(val.target.value)
                }}
                InputProps={{
                  startAdornment: <EmailIcon />,
                }}
                classes={{
                  root: classes.textField,
                }}
                label="Email"
                variant="outlined"
              />
            </Grid>
            <Grid item xs={12}>
              <Box pt={5} />
              <TextField
                defaultValue={password}
                onChange={(val) => {
                  setPassword(val.target.value)
                }}
                InputProps={{
                  startAdornment: <LockIcon />,
                }}
                classes={{
                  root: classes.textField,
                }}
                label="Password"
                variant="outlined"
              />
            </Grid>
          </Grid>
          {error && <p style={{ color: "black" }}>Error</p>}
          <Grid item xs={12}>
            <Box pt={3}>
              <Button
                disabled={!email || !password}
                onClick={onClick}
                style={{ borderRadius: "35px", width: "35%" }}
                variant="contained"
                color="primary"
              >
                Submit
              </Button>
            </Box>
          </Grid>
        </Box>
      </Paper>
    </Box>
  )
}

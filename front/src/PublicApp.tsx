import React from "react"
import { Redirect, Route, Switch } from "react-router-dom"

import AppLayout from "./components/layout"
import { URLS } from "./routes"
import Login from "./scenes/Login/"
import Register from "./scenes/Register/"

const PublicApp = (): JSX.Element => {
  return (
    <AppLayout>
      <Switch>
        <Route exact path={URLS.Account.Register} component={Register} />
        <Route exact path={URLS.Account.Login} component={Login} />
        <Redirect
          to={{
            pathname: URLS.Account.Login,
          }}
        />
      </Switch>
    </AppLayout>
  )
}

export default PublicApp

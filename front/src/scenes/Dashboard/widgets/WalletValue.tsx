import {
  Paper,
  Box,
  Typography,
  TableCell,
  Table,
  TableRow,
  TableHead,
  TableBody,
  makeStyles,
  Theme,
  Grid,
  Avatar,
  TableContainer,
} from "@material-ui/core"
import React, { useState } from "react"
import { ResponsivePie } from "@nivo/pie"
import { useQuery } from "@apollo/client"
import { GET_WIDGET_DATA } from "../../../services/api/querys"
import BigNumber from "bignumber.js"
import LoadingSpinner from "../../../components/misc/LoadingSpinner"
import dayjs from "dayjs"
import { ResponsiveLine, Line } from "@nivo/line"

const useStyles = makeStyles((theme: Theme) => ({}))

const commonProperties = {
  width: 900,
  height: 400,
  margin: { top: 20, right: 20, bottom: 60, left: 80 },
  animate: true,
  enableSlices: "x",
}

interface Data {
  x: string
  y: number
}

export default function Transfers() {
  const classes = useStyles()
  const [data, setData] = useState<Data[]>([{ x: "2021-05-27".toString(), y: 39525.49807 }])
  const widgetData = useQuery(GET_WIDGET_DATA, {
    variables: { data: { name: "Wallet value over the month" } },
    onCompleted: async (res) => {
      const result = JSON.parse(res.widget.result)
      const tmp = []
      for (const key in result) {
        tmp.push({ x: key.split("T")[0], y: result[key] })
      }
      setData(tmp)
    },
  })

  if (widgetData.loading) return <LoadingSpinner />
  return (
    <Paper style={{ width: 885 }}>
      <Box pb={2} style={{ textAlign: "center" }}>
        <Typography component="h1">Wallet value</Typography>
      </Box>
      <Box style={{ height: "360px" }}>
        <ResponsiveLine
          {...commonProperties}
          enableArea={true}
          data={[
            {
              id: "USD Value",
              data: data,
            },
          ]}
          xScale={{
            type: "time",
            format: "%Y-%m-%d",
            useUTC: false,
            precision: "day",
          }}
          xFormat="time:%Y-%m-%d"
          yScale={{
            type: "linear",
            stacked: false,
          }}
          axisLeft={{
            legend: "USD value",
            legendOffset: 12,
          }}
          axisBottom={{
            format: "%b %d",
            tickValues: "every 2 days",
            legend: "time scale",
            legendOffset: -12,
          }}
          enablePointLabel={false}
          pointSize={10}
          pointBorderWidth={1}
          pointBorderColor={{
            from: "color",
            modifiers: [["darker", 0.3]],
          }}
          useMesh={true}
          enableSlices={false}
        />
      </Box>
    </Paper>
  )
}

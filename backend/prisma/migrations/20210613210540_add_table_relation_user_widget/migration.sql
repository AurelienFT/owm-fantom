/*
  Warnings:

  - You are about to drop the column `widgets` on the `User` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "User" DROP COLUMN "widgets";

-- CreateTable
CREATE TABLE "UserWidget" (
    "userId" INTEGER NOT NULL,
    "widgetId" INTEGER NOT NULL,

    PRIMARY KEY ("userId","widgetId")
);

-- AddForeignKey
ALTER TABLE "UserWidget" ADD FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "UserWidget" ADD FOREIGN KEY ("widgetId") REFERENCES "Widget"("id") ON DELETE CASCADE ON UPDATE CASCADE;

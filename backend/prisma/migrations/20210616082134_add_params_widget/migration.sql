/*
  Warnings:

  - Added the required column `params` to the `UserWidget` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "UserWidget" ADD COLUMN     "params" JSONB NOT NULL;

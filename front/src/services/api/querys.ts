import { gql } from "@apollo/client"

const CONNECT_WITH_EMAIL = gql`
  query login($data: LoginInput!) {
    login(data: $data) {
      user {
        email
        name
        id
      }
      jwt
    }
  }
`
const GET_WIDGETS_NAME = gql`
  query widgets {
    widgets {
      id
      name
      type
    }
  }
`

const GET_MY_WIDGETS = gql`
  query myWidgets {
    myWidgets {
      __typename
      x
      y
      widget {
        name
      }
    }
  }
`

const GET_WIDGET_DATA = gql`
  query widget($data: WidgetExecInput!) {
    widget(data: $data) {
      result
    }
  }
`

export { CONNECT_WITH_EMAIL, GET_WIDGETS_NAME, GET_MY_WIDGETS, GET_WIDGET_DATA }

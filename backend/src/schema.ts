import { makeExecutableSchema } from '@graphql-tools/schema'
import { Context } from './context'
import bcrypt from 'bcrypt'
import { User, UserWidget } from '@prisma/client'
import jsonwebtoken from 'jsonwebtoken'
import { UserCreateInput, LoginInput } from './interfaces/User'
import { UserWidgetInput, UserWidgetPositionsInput, UserWidgetDeleteInput } from './interfaces/UserWidget'
import { WidgetExecInput, WidgetResult } from './interfaces/Widget'
import balance from './widgets/balance'
import walletValue from './widgets/walletValue'
import spiritSwapPositions from './widgets/spiritSwapPositions'
import spiritSwapTransactions from './widgets/spiritSwapTransactions'
import transfers from './widgets/transfers'

const typeDefs = `
type Mutation {
    signupUser(data: UserCreateInput!): LoginResponse!
    addNewWidget(data: UserWidgetInput!): User!
    removeWidget(data: UserWidgetDeleteInput!): User!
    updatePositionsUserWidget(data: UserWidgetPositionsInput!): UserWidget!
}
type Query {
  allUsers: [User!]!
  myWidgets: [UserWidget]!
  login(data: LoginInput!): LoginResponse!
  widget(data: WidgetExecInput!): WidgetResult!
  widgets: [Widget]!
}
type User {
  email: String!
  id: Int!
  name: String
  widgets: [UserWidget]!
}
type Widget {
  id: Int!
  name: String!
  type: TypeWidgetResult!
}
type UserWidget {
  userId: Int!
  widgetId: Int!
  widget: Widget!
  user: User!
  x: Int!
  y: Int!
}
input UserCreateInput {
  email: String!
  name: String!
  password: String!
}
input LoginInput {
  email: String!
  password: String!
}
input UserWidgetInput {
  widgetId: Int!
  x: Int!
  y: Int!
  params: String!
}
input UserWidgetDeleteInput {
  widgetId: Int!
}
type LoginResponse {
  user: User!
  jwt: String!
}
input WidgetExecInput {
  name: String!
}
enum TypeWidgetResult {
  NUMBER,
  GRAPHIC,
  ARRAY
}
type WidgetResult {
  id: Int!
  name: String!
  result: String!
  type: TypeWidgetResult!
}
input UserWidgetPositionsInput {
  widgetId: Int!
  x: Int
  y: Int
}
`

const resolvers = {
  Query: {
    allUsers: (_parent: any, _args: any, context: Context) => {
      return context.prisma.user.findMany()
    },
    login: async (_parent: any, args: { data: LoginInput }, context: Context) => {
      let user = await context.prisma.user.findUnique({ where: { email: args.data.email } });
      if (user && await bcrypt.compare(args.data.password, user.password)) {
        return {
          user,
          jwt: await jsonwebtoken.sign(user.id.toString(), "charleslebg")
        }
      }
    },
    widget: async (_parent: any, args: { data: WidgetExecInput }, context: Context) => {
      if (!context.user) {
        throw new Error("Please call with a JWT.");
      }
      console.log(context.user);
      let widget = await context.prisma.widget.findUnique({
        where: {
          name: args.data.name
        }
      });
      let userWidget = await context.prisma.userWidget.findUnique({
        where: {
          userId_widgetId: {
            userId: context.user?.id,
            widgetId: widget.id
          }
        }
      });
      switch (args.data.name) {
        case "Balance":
          return await balance(context.user, userWidget.params);
        case "Wallet value over the month":
          return await walletValue(context.user, userWidget.params);
        case "SpiritSwap positions":
          return await spiritSwapPositions(context.user, userWidget.params);
        case "SpiritSwap transactions":
          return await spiritSwapTransactions(context.user, userWidget.params);
        case "Transfers":
          return await transfers(context.user, userWidget.params);
        default:
          throw new Error("Widget not supported.");
      }
    },
    widgets: async (_parent: any, _args: null, context: Context) => {
      return await context.prisma.widget.findMany();
    },
    myWidgets: async (_parent: any, _args: null, context: Context) => {
      if (!context.user) {
        throw new Error("Please call with a JWT.");
      }
      return await context.prisma.userWidget.findMany({
        where: {
          userId: context.user.id
        }
      });
    }
  },
  Mutation: {
    signupUser: async (_parent: any, args: { data: UserCreateInput }, context: Context) => {
      let user = await context.prisma.user.create({
        data: {
          name: args.data.name,
          email: args.data.email,
          password: await bcrypt.hash(args.data.password, 10)
        },
      });
      return {
        user,
        jwt: await jsonwebtoken.sign(user.id.toString(), "charleslebg")
      }
    },
    addNewWidget: async (_parent: any, args: { data: UserWidgetInput }, context: Context) => {
      if (!context.user) {
        throw new Error("Please call with a JWT.");
      }
      await context.prisma.userWidget.create({
        data: {
          x: args.data.x,
          y: args.data.y,
          user: {
            connect: {
              id: context.user.id
            }
          },
          params: JSON.parse(args.data.params),
          widget: {
            connect: {
              id: args.data.widgetId
            }
          }
        }
      });
      return context.user
    },
    removeWidget: async (_parent: any, args: { data: UserWidgetDeleteInput }, context: Context) => {
      if (!context.user) {
        throw new Error("Please call with a JWT.");
      }
      await context.prisma.userWidget.delete({
        where: {
          userId_widgetId: {
            userId: context.user.id,
            widgetId: args.data.widgetId
          }
        }
      });
      return context.user
    },
    updatePositionsUserWidget: async (_parent: any, args: { data: UserWidgetPositionsInput }, context: Context) => {
      if (!context.user) {
        throw new Error("Please call with a JWT.");
      }
      return await context.prisma.userWidget.update({
        where: {
          userId_widgetId: {
            userId: context.user.id,
            widgetId: args.data.widgetId
          }
        },
        data: {
          x: args.data.x,
          y: args.data.y
        }
      });
    }
  },
  User: {
    widgets: (parent: User, _args: any, context: Context) => {
      return context.prisma.user.findUnique({
        where: { id: parent?.id }
      }).widgets()
    }
  },
  UserWidget: {
    widget: (parent: UserWidget, _args: any, context: Context) => {
      return context.prisma.widget.findUnique({
        where: { id: parent.widgetId }
      });
    },
    user: (parent: UserWidget, _args: any, context: Context) => {
      return context.prisma.user.findUnique({
        where: { id: parent.userId }
      });
    }
  }
}

export const schema = makeExecutableSchema({
  resolvers,
  typeDefs,
})

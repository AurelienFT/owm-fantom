import { PaletteType } from "@material-ui/core"
import { createMuiTheme } from "@material-ui/core/styles"
// @ts-ignore
import type {} from "@material-ui/lab/themeAugmentation"

const PrimaryColor = "#FC636B"
const SecondaryColor = "#34495e"
const BackgroundColor = "#FAFAFC"
const AlternateBackgroundColor = "#D9D9DD"

export const TextColor = PrimaryColor
export const GreenColor = "#27ae60"
export const RedColor = "#FA4739"
export const White = "#FFF"
export const LightGrey = "#DDD"
export const DarkGrey = "#404040"
export const LightBlue = "#2FAEDC"
export const Orange = "#FA8B39"

export const GainColor = GreenColor
export const LossColor = RedColor

export const WarningColor = "#e67e22"

export const ErrorColor = RedColor

export const LinkColor = "#1e70bf"

// custom theme for this app
const getTheme = (paletteType: PaletteType) =>
  createMuiTheme({
    props: {
      MuiDrawer: {
        anchor: "right",
      },
      MuiPopover: {
        keepMounted: true,
      },
      MuiButtonBase: {
        disableRipple: true,
      },
      MuiButton: {
        variant: "contained",
        color: "primary",
      },
      MuiTextField: {
        variant: "outlined",
      },
      MuiSwitch: {
        color: "primary",
      },
    },
    overrides: {
      MuiListItem: {
        root: {
          // weird yellow focus navbar
          "&.Mui-focusVisible": {
            backgroundColor: PrimaryColor,
          },
        },
      },
      MuiAvatar: {
        square: {
          boxShadow:
            "rgba(0, 0, 0, 0.2) 0px 3px 1px -2px, rgba(0, 0, 0, 0.14) 0px 2px 2px 0px, rgba(0, 0, 0, 0.12) 0px 1px 5px 0px",
        },
      },
      MuiFormControl: {
        root: {
          borderColor: PrimaryColor,
        },
      },
      MuiFormLabel: {
        root: {
          fontSize: "1.1rem",
        },
      },
      MuiFormHelperText: {
        contained: {
          marginLeft: 0,
        },
      },
      MuiCheckbox: {
        colorPrimary: {
          color: PrimaryColor,
        },
      },
      MuiButton: {
        root: {
          textTransform: "none",
        },
      },
      MuiOutlinedInput: {
        input: {
          padding: "12px",
        },
      },
      MuiPaper: {
        root: {
          backgroundColor: "#FAFAFC",
          border: "2px solid rgba(100, 100, 100, 0.1)",
        },
        elevation1: {
          boxShadow: "none",
        },
      },
      MuiTooltip: {
        tooltip: {
          backgroundColor: "white",
          color: PrimaryColor,
          borderColor: AlternateBackgroundColor,
          border: `1px solid`,
          boxShadow: "2px 2px 15px 0 rgba(0,0,0,0.15)",
          fontSize: "0.9rem",
        },
      },
      MuiMenuItem: {
        root: {
          // HACK: Without this Badges are cut off in MenuItem.
          // Material UI issue #21639
          // TODO: Remove this when upgrading to Material UI 5.0.
          overflow: "visible",
        },
      },
    },
    shape: {
      borderRadius: 2,
    },
    palette: {
      type: paletteType,
      primary: {
        main: PrimaryColor,
      },
      secondary: {
        main: SecondaryColor,
      },
      background: {
        default: BackgroundColor,
      },
      text: {
        primary: TextColor,
        disabled: "#9E9E9E",
      },
      action: {
        selected: SecondaryColor,
      },
    },
    typography: {
      fontFamily: `"Mukta"`,
    },
  })

export default getTheme

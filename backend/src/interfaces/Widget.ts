export interface WidgetExecInput {
    name: string
}

export enum TypeWidgetResult {
    NUMBER = "NUMBER",
    GRAPHIC = "GRAPHIC",
    ARRAY = "ARRAY"
}

export interface WidgetResult {
    id: number,
    name: string,
    result: string,
    type: TypeWidgetResult
}

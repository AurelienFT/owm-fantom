import { Paper, Box, Typography } from "@material-ui/core"
import React, { useState } from "react"
import { ResponsivePie } from "@nivo/pie"
import { useQuery } from "@apollo/client"
import { GET_WIDGET_DATA } from "../../../services/api/querys"
import BigNumber from "bignumber.js"
import LoadingSpinner from "../../../components/misc/LoadingSpinner"

export default function Balance() {
  const [data, setData] = useState([])
  const widgetData = useQuery(GET_WIDGET_DATA, {
    variables: { data: { name: "Balance" } },
    onCompleted: async (res) => {
      const result = JSON.parse(res.widget.result)
      let clean = result.map((x: any) => {
        return {
          label: x.contract_ticker_symbol,
          id: x.contract_ticker_symbol,
          value:
            x.contract_decimals < 1
              ? new BigNumber(x.balance).decimalPlaces(4, BigNumber.ROUND_UP).toNumber()
              : new BigNumber(x.balance)
                  .shiftedBy(-x.contract_decimals)
                  .decimalPlaces(4, BigNumber.ROUND_UP)
                  .toNumber(),
        }
      })
      clean = clean.filter((x: any) => x.value > 0).slice(0, 10)
      setData(clean)
    },
  })
  if (widgetData.loading) return <LoadingSpinner />
  return (
    <Paper style={{ width: 500 }}>
      <Box style={{ height: "300px" }}>
        <Box pb={2} style={{ textAlign: "center" }}>
          <Typography component="h1">Balance</Typography>
        </Box>
        <ResponsivePie
          theme={{
            legends: { hidden: { symbol: { fill: "#d9d9d9" } } },
          }}
          data={data}
          margin={{ right: 40, bottom: 50, left: 40 }}
          colors={{ scheme: "spectral" }}
          innerRadius={0.6}
          padAngle={0.7}
          cornerRadius={3}
          activeOuterRadiusOffset={8}
          borderWidth={1}
          borderColor={{ from: "color", modifiers: [["darker", 0.2]] }}
          enableArcLinkLabels={false}
          enableArcLabels={false}
          legends={[
            {
              anchor: "right",
              direction: "column",
              justify: false,
              translateX: 25,
              translateY: 0,
              itemsSpacing: 10,
              itemWidth: 100,
              itemHeight: 15,
              itemDirection: "left-to-right",
              itemOpacity: 1,
              symbolSize: 18,
              symbolShape: "circle",
              toggleSerie: true,
              effects: [
                {
                  on: "hover",
                  style: {
                    itemTextColor: "#999",
                  },
                },
              ],
            },
          ]}
        />
      </Box>
    </Paper>
  )
}

export const URLS = {
  Account: {
    Login: "/login",
    Register: "/register",
  },
  Dashboard: "/dashboard",
}
